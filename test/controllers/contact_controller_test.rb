require 'test_helper'

class ContactControllerTest < ActionDispatch::IntegrationTest
  test "should get view" do
    get contact_view_url
    assert_response :success
  end

  test "should get new" do
    get contact_new_url
    assert_response :success
  end

  test "should get create" do
    get contact_create_url
    assert_response :success
  end

  test "should get delete" do
    get contact_delete_url
    assert_response :success
  end

  test "should get edit" do
    get contact_edit_url
    assert_response :success
  end

end
