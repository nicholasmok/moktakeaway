Rails.application.routes.draw do
  get 'contacts/:id', to: 'contacts#show'
  get 'contacts/new'
  get 'contacts/create'
  get 'contacts/delete'
  get 'contacts/edit'
  resources :company_contact_relationships
  root to: 'companies#index'
  get 'companies/index'
  get 'companies/:id', to: 'companies#show'
  get 'companies/:company_id/contacts', to: 'contacts#index'
  get 'companies/:company_id/contact/add-relationship/:contact_id', to: 'contacts#add_relationship'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
