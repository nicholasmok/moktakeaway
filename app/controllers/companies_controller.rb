class CompaniesController < ApplicationController
  def index
    @companies = Company.sorted_alphabetically
  end

  def show
    @company = Company.find_by_id(params[:id])
  end
end
