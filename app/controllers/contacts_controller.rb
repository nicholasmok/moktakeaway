class ContactsController < ApplicationController
  def index
    #Show company contacts given the company id
    @company_id = params[:company_id]
    @company_contacts = Company.find_by_id(@company_id).contacts
    
    @contact_relationships = Contact.where(id: [1,3])

  end
  
  def show
    @contact = Contact.find_by_id(params[:id])
  end

  def add_relationship
    @person = Contact.find_by_id(params[:contact_id])
    @relationship = CompanyContactRelationship.new
    @relationship.contact_id = params[:contact_id]
    puts @relationship.contact_id
    #generate list of relationships that cannot be had. ie. existing ones and self
    current_relationship_ids = @person.company_contact_relationships.select(:related_contact_id).map { |c| c.related_contact_id }
    offlimits_ids = current_relationship_ids.push(params[:contact_id])
    @potential_relationships = Contact.where.not(id: offlimits_ids).select(:id, :first_name, :last_name).map {|c| [ c.first_name + " " + c.last_name, c.id] }
  end

  def new
  end

  def create
  end

  def delete
  end

  def edit
  end
end
