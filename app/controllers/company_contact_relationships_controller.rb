class CompanyContactRelationshipsController < ApplicationController

    def create 
        params.permit!
        
        @relationship = CompanyContactRelationship.new(params[:company_contact_relationship])
        contact = Contact.find_by_id(params[:company_contact_relationship][:contact_id])
        
        if(@relationship.save)
            redirect_to "/companies/" + contact.company_id.to_s + "/contacts"
        end
    
    end

    
end
