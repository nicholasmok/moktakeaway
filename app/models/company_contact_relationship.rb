class CompanyContactRelationship < ApplicationRecord
    belongs_to :contacts, optional: true
end
