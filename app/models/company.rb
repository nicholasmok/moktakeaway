class Company < ApplicationRecord
    has_many :contacts

    scope :sorted_alphabetically, lambda {order("company_name ASC")}
end
