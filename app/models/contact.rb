class Contact < ApplicationRecord
    belongs_to :company
    has_many :company_contact_relationships
end
