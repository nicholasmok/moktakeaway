-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: moktakeaway_development
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ar_internal_metadata`
--

DROP TABLE IF EXISTS `ar_internal_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ar_internal_metadata` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ar_internal_metadata`
--

LOCK TABLES `ar_internal_metadata` WRITE;
/*!40000 ALTER TABLE `ar_internal_metadata` DISABLE KEYS */;
INSERT INTO `ar_internal_metadata` VALUES ('environment','development','2019-08-18 02:44:57.790513','2019-08-18 02:44:57.790513');
/*!40000 ALTER TABLE `ar_internal_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) DEFAULT NULL,
  `company_description` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES (1,'Company 1',NULL,'2019-08-18 03:02:38.371853','2019-08-18 03:02:38.371853');
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_contact_relationships`
--

DROP TABLE IF EXISTS `company_contact_relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_contact_relationships` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `related_contact_id` int(11) DEFAULT NULL,
  `relationship_type` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `relationship_index` (`contact_id`,`related_contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_contact_relationships`
--

LOCK TABLES `company_contact_relationships` WRITE;
/*!40000 ALTER TABLE `company_contact_relationships` DISABLE KEYS */;
INSERT INTO `company_contact_relationships` VALUES (1,1,3,'Married','2019-08-18 09:51:57.342440','2019-08-18 09:51:57.342440'),(2,3,1,'Married','2019-08-18 09:53:54.010352','2019-08-18 09:53:54.010352'),(3,2,3,'Sibling','2019-08-18 10:10:06.625628','2019-08-18 10:10:06.625628'),(4,3,2,'Sibling','2019-08-18 10:11:46.235809','2019-08-18 10:11:46.235809'),(8,4,45,'Parent','2019-08-19 03:01:05.793681','2019-08-19 03:01:05.793681'),(9,45,4,'Child','2019-08-19 03:01:32.399508','2019-08-19 03:01:32.399508');
/*!40000 ALTER TABLE `company_contact_relationships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_contacts_on_company_id` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (1,'John','Doe','M',1,'2019-08-18 03:04:56.831593','2019-08-18 03:04:56.831593'),(2,'John','Franks',NULL,1,'2019-08-18 03:35:19.489440','2019-08-18 10:10:10.748653'),(3,'Jane','Doe',NULL,1,'2019-08-18 03:45:04.561213','2019-08-18 03:45:04.561213'),(4,'Albert','Einstein',NULL,1,'2019-08-19 02:57:42.489649','2019-08-19 02:57:42.489649'),(5,'James','Page',NULL,1,'2019-08-19 02:58:04.132230','2019-08-19 02:58:04.132230'),(6,'Harriette','Prum',NULL,1,'2019-08-19 03:00:44.151389','2019-08-19 03:00:44.151389'),(7,'Filomena','Meza',NULL,1,'2019-08-19 03:00:44.161316','2019-08-19 03:00:44.161316'),(8,'Kathryne','Dople',NULL,1,'2019-08-19 03:00:44.173513','2019-08-19 03:00:44.173513'),(9,'Cristal','Ellwood',NULL,1,'2019-08-19 03:00:44.184793','2019-08-19 03:00:44.184793'),(10,'Ruthanne','Burling',NULL,1,'2019-08-19 03:00:44.195087','2019-08-19 03:00:44.195087'),(11,'Cherilyn','Sipe',NULL,1,'2019-08-19 03:00:44.206129','2019-08-19 03:00:44.206129'),(12,'Mika','Miedema',NULL,1,'2019-08-19 03:00:44.216381','2019-08-19 03:00:44.216381'),(13,'Ryann','Dionisio',NULL,1,'2019-08-19 03:00:44.226481','2019-08-19 03:00:44.226481'),(14,'Royal','Chamberlin',NULL,1,'2019-08-19 03:00:44.237082','2019-08-19 03:00:44.237082'),(15,'Francene','Trantham',NULL,1,'2019-08-19 03:00:44.253194','2019-08-19 03:00:44.253194'),(16,'Toney','Laura',NULL,1,'2019-08-19 03:00:44.263570','2019-08-19 03:00:44.263570'),(17,'Delicia','Hutcheson',NULL,1,'2019-08-19 03:00:44.278021','2019-08-19 03:00:44.278021'),(18,'Sirena','Batres',NULL,1,'2019-08-19 03:00:44.287769','2019-08-19 03:00:44.287769'),(19,'Alonso','Shawgo',NULL,1,'2019-08-19 03:00:44.303122','2019-08-19 03:00:44.303122'),(20,'Whitney','Sherry',NULL,1,'2019-08-19 03:00:44.312345','2019-08-19 03:00:44.312345'),(21,'Valentin','Mervis',NULL,1,'2019-08-19 03:00:44.322034','2019-08-19 03:00:44.322034'),(22,'Tesha','Gover',NULL,1,'2019-08-19 03:00:44.337590','2019-08-19 03:00:44.337590'),(23,'Zonia','Eckert',NULL,1,'2019-08-19 03:00:44.346067','2019-08-19 03:00:44.346067'),(24,'Mia','Mcnair',NULL,1,'2019-08-19 03:00:44.354976','2019-08-19 03:00:44.354976'),(25,'Georgianne','Bono',NULL,1,'2019-08-19 03:00:44.364082','2019-08-19 03:00:44.364082'),(26,'King','Bollman',NULL,1,'2019-08-19 03:00:44.376803','2019-08-19 03:00:44.376803'),(27,'Kareen','Myres',NULL,1,'2019-08-19 03:00:44.388500','2019-08-19 03:00:44.388500'),(28,'Gwenn','Brassell',NULL,1,'2019-08-19 03:00:44.401807','2019-08-19 03:00:44.401807'),(29,'Jefferson','Zupan',NULL,1,'2019-08-19 03:00:44.410884','2019-08-19 03:00:44.410884'),(30,'Ollie','Cabello',NULL,1,'2019-08-19 03:00:44.417588','2019-08-19 03:00:44.417588'),(31,'Shayla','Preble',NULL,1,'2019-08-19 03:00:44.427972','2019-08-19 03:00:44.427972'),(32,'Jeana','Kinnaman',NULL,1,'2019-08-19 03:00:44.436427','2019-08-19 03:00:44.436427'),(33,'Love','Meek',NULL,1,'2019-08-19 03:00:44.446130','2019-08-19 03:00:44.446130'),(34,'Garrett','Juntunen',NULL,1,'2019-08-19 03:00:44.455018','2019-08-19 03:00:44.455018'),(35,'Ilse','Patch',NULL,1,'2019-08-19 03:00:44.463277','2019-08-19 03:00:44.463277'),(36,'Shantel','Rispoli',NULL,1,'2019-08-19 03:00:44.474566','2019-08-19 03:00:44.474566'),(37,'Bethanie','Mallow',NULL,1,'2019-08-19 03:00:44.484529','2019-08-19 03:00:44.484529'),(38,'Denita','Vanwagenen',NULL,1,'2019-08-19 03:00:44.496495','2019-08-19 03:00:44.496495'),(39,'Darrell','Flores',NULL,1,'2019-08-19 03:00:44.507556','2019-08-19 03:00:44.507556'),(40,'Maire','Halliday',NULL,1,'2019-08-19 03:00:44.520905','2019-08-19 03:00:44.520905'),(41,'Ardell','Mascio',NULL,1,'2019-08-19 03:00:44.529910','2019-08-19 03:00:44.529910'),(42,'Charley','Rick',NULL,1,'2019-08-19 03:00:44.538441','2019-08-19 03:00:44.538441'),(43,'Karmen','Welte',NULL,1,'2019-08-19 03:00:44.551036','2019-08-19 03:00:44.551036'),(44,'Mary','Parisi',NULL,1,'2019-08-19 03:00:44.561181','2019-08-19 03:00:44.561181'),(45,'Isreal','Rolfes',NULL,1,'2019-08-19 03:00:44.570977','2019-08-19 03:00:44.570977'),(46,'Dong','Asaro',NULL,1,'2019-08-19 03:00:44.577866','2019-08-19 03:00:44.577866'),(47,'Quiana','Biro',NULL,1,'2019-08-19 03:00:44.591155','2019-08-19 03:00:44.591155'),(48,'Dorathy','Iliff',NULL,1,'2019-08-19 03:00:44.609762','2019-08-19 03:00:44.609762'),(49,'Laveta','Horstman',NULL,1,'2019-08-19 03:00:44.618622','2019-08-19 03:00:44.618622'),(50,'Sunday','Salisbury',NULL,1,'2019-08-19 03:00:44.625395','2019-08-19 03:00:44.625395'),(51,'Ching','Mangual',NULL,1,'2019-08-19 03:00:44.636631','2019-08-19 03:00:44.636631'),(52,'Davida','Herwig',NULL,1,'2019-08-19 03:00:44.645380','2019-08-19 03:00:44.645380'),(53,'Malik','Nagel',NULL,1,'2019-08-19 03:00:44.653238','2019-08-19 03:00:44.653238'),(54,'Audria','Kubat',NULL,1,'2019-08-19 03:00:44.662363','2019-08-19 03:00:44.662363');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20190818022019'),('20190818022204'),('20190818022329'),('20190818025028'),('20190818025134'),('20190818032610'),('20190818071742');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-19 13:03:19
