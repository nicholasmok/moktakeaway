class AddIndexesForCompanyContactRelationships < ActiveRecord::Migration[6.0]
  def change
    add_index("company_contact_relationships", "contact_id")
    add_index("company_contact_relationships", "related_contact_id")
  end
end
