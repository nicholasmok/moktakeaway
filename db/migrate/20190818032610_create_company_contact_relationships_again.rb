class CreateCompanyContactRelationshipsAgain < ActiveRecord::Migration[6.0]
  def change
    create_table :company_contact_relationships do |t|
      t.integer :contact_id
      t.integer :related_contact_id
      t.string :relationship_type

      t.timestamps
    end
    add_index("company_contact_relationships", ["contact_id", "related_contact_id"], :name => "relationship_index")
  end
end
