class AddIndexForContacts < ActiveRecord::Migration[6.0]
  def change
    add_index("contacts", "company_id")
  end
end
