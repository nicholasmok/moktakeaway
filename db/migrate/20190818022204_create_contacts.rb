class CreateContacts < ActiveRecord::Migration[6.0]
  def change
    create_table :contacts do |t|
      t.string :first_name
      t.string :last_name
      t.string :gender, :limit => 1
      t.integer :company_id

      t.timestamps
    end
  end
end
